/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dragrace;

import java.awt.Image;
import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;


class evostock extends mobilparent implements Serializable{

    public evostock() {
        this.cekStyle = 0;
        this.cekStat = 0;
        this.maxspeed = 250;
        this.hp = 193;
        this.weight = 1482;
        this.boost = 0;
        this.rawImage = new ImageIcon("image/evo-stock-garage.png");
        this.raceImage = new ImageIcon("image/evo-stock-race.png");
    }
}
class evomod1 extends evostock implements Serializable{

    public evomod1() {
        this.rawImage = new ImageIcon("image/evo-mod1-garage.png");
        this.raceImage = new ImageIcon("image/evo-mod1-race.png");
    }
}

class evomod2 extends evomod1 implements Serializable{

    public evomod2() {
        this.maxspeed = maxspeed;
        this.hp = hp;
        this.weight = weight;
        this.boost = boost;
        this.rawImage = new ImageIcon("image/evo-mod2-garage.png");
        this.raceImage = new ImageIcon("image/evo-mod2-race.png");
    }
}

class ft86stock extends mobilparent implements Serializable{

    public ft86stock() {
        this.cekStyle = 0;
        this.cekStat = 0;
        this.maxspeed = 200;
        this.hp = 200;
        this.weight = 1298;
        this.boost = 0;
        this.rawImage = new ImageIcon("image/86-stock-garage.png");
        this.raceImage = new ImageIcon("image/86-stock-race.png");
    }
}
class ft86mod1 extends ft86stock implements Serializable{

    public ft86mod1() {
        this.maxspeed = maxspeed;
        this.hp = hp;
        this.weight = weight;
        this.boost = boost;
        this.rawImage = new ImageIcon("image/86-mod1-garage.png");
        this.raceImage = new ImageIcon("image/86-mod1-race.png");
    }
}

class ft86mod2 extends ft86mod1 implements Serializable{

    public ft86mod2() {
        this.maxspeed = maxspeed;
        this.hp = hp;
        this.weight = weight;
        this.boost = boost;
        this.rawImage = new ImageIcon("image/86-mod2-garage.png");
        this.raceImage = new ImageIcon("image/86-mod2-race.png");
    }
}

class nissan370stock extends mobilparent implements Serializable{

    public nissan370stock() {
        this.cekStyle = 0;
        this.cekStat = 0;
        this.maxspeed = 249;
        this.hp = 350;
        this.weight = 1466;
        this.boost = 0;
        rawImage = new ImageIcon("image/370z-stock-garage.png");
        this.raceImage = new ImageIcon("image/370z-stock-race.png");
    }
}
class nissan370mod1 extends nissan370stock implements Serializable{

    public nissan370mod1() {
        this.maxspeed = maxspeed;
        this.hp = hp;
        this.weight = weight;
        this.boost = boost;
        rawImage = new ImageIcon("image/370z-mod1-garage.png");
        this.raceImage = new ImageIcon("image/370z-mod1-race.png");
    }
}

class nissan370mod2 extends nissan370mod1 implements Serializable{

    public nissan370mod2() {
        this.maxspeed = maxspeed;
        this.hp = hp;
        this.weight = weight;
        this.boost = boost;
        rawImage = new ImageIcon("image/370z-mod2-garage.png");
        this.raceImage = new ImageIcon("image/370z-mod2-race.png");
    }
}

class bentleystock extends mobilparent implements Serializable{
    public bentleystock() {
        this.cekStyle = 0;
        this.cekStat = 0;
        this.maxspeed = 345;
        this.hp = 700;
        this.weight = 2320;
        this.boost = 0;
        this.rawImage = new ImageIcon("image/bentley-stock-garage.png");
        this.raceImage = new ImageIcon("image/bentley-stock-race.png");
    }
}
class bentleymod1 extends bentleystock implements Serializable{

    public bentleymod1() {
        this.maxspeed = maxspeed;
        this.hp = hp;
        this.weight = weight;
        this.boost = boost;
        this.rawImage = new ImageIcon("image/bentley-mod1-garage.png");
        this.raceImage = new ImageIcon("image/bentley-mod1-race.png");
    }
}

class bentleymod2 extends bentleymod1 implements Serializable{

    public bentleymod2() {
        this.maxspeed = maxspeed;
        this.hp = hp;
        this.weight = weight;
        this.boost = boost;
        this.rawImage = new ImageIcon("image/bentley-mod2-garage.png");
        this.raceImage = new ImageIcon("image/bentley-mod2-race.png");
    }
}

class c63stock extends mobilparent implements Serializable{

    public c63stock() {
        this.cekStyle = 0;
        this.cekStat = 0;
        this.maxspeed = 320;
        this.hp = 469;
        this.weight = 1730;
        this.boost = 0;
        this.rawImage = new ImageIcon("image/c63-stock-garage.png");
        this.raceImage = new ImageIcon("image/c63-stock-race.png");
    }
}
class c63mod1 extends c63stock implements Serializable{

    public c63mod1() {
        this.maxspeed = maxspeed;
        this.hp = hp;
        this.weight = weight;
        this.boost = boost;
        this.rawImage = new ImageIcon("image/c63-mod1-garage.png");
        this.raceImage = new ImageIcon("image/c63-mod1-race.png");
    }
}

class c63mod2 extends c63mod1 implements Serializable{

    public c63mod2() {
        this.maxspeed = maxspeed;
        this.hp = hp;
        this.weight = weight;
        this.boost = boost;
        this.rawImage = new ImageIcon("image/c63-mod2-garage.png");
        this.raceImage = new ImageIcon("image/c63-mod2-race.png");
    }
}

class m4stock extends mobilparent implements Serializable{

    public m4stock() {
        this.cekStyle = 0;
        this.cekStat = 0;
        this.maxspeed = 305;
        this.hp = 425;
        this.weight = 1572;
        this.boost = 0;
        this.rawImage = new ImageIcon("image/m4-stock-garage.png");
        this.raceImage = new ImageIcon("image/m4-stock-race.png");
    }
}
class m4mod1 extends m4stock implements Serializable{

    public m4mod1() {
        this.maxspeed = maxspeed;
        this.hp = hp;
        this.weight = weight;
        this.boost = boost;
        this.rawImage = new ImageIcon("image/m4-mod1-garage.png");
        this.raceImage = new ImageIcon("image/m4-mod1-race.png");
    }
}

class m4mod2 extends m4mod1 implements Serializable{

    public m4mod2() {
        this.maxspeed = maxspeed;
        this.hp = hp;
        this.weight = weight;
        this.boost = boost;
        this.rawImage = new ImageIcon("image/m4-mod2-garage.png");
        this.raceImage = new ImageIcon("image/m4-mod2-race.png");
    }
}

class r8stock extends mobilparent implements Serializable{

    public r8stock() {
        this.cekStyle = 0;
        this.cekStat = 0;
        this.maxspeed = 316;
        this.hp = 540;
        this.weight = 1525;
        this.boost = 0;
        this.rawImage = new ImageIcon("image/r8-stock-garage.png");
        this.raceImage = new ImageIcon("image/r8-stock-race.png");
    }
}
class r8mod1 extends r8stock implements Serializable{
    
    public r8mod1() {
        this.maxspeed = maxspeed;
        this.hp = hp;
        this.weight = weight;
        this.boost = boost;
        this.rawImage = new ImageIcon("image/r8-mod1-garage.png");
        this.raceImage = new ImageIcon("image/r8-mod1-race.png");
    }
}

class r8mod2 extends r8mod1 implements Serializable{

    public r8mod2() {
        this.maxspeed = maxspeed;
        this.hp = hp;
        this.weight = weight;
        this.boost = boost;
        this.rawImage = new ImageIcon("image/r8-mod2-garage.png");
        this.raceImage = new ImageIcon("image/r8-mod2-race.png");
    }
}

class suprastock extends mobilparent implements Serializable{

    public suprastock() {
        this.cekStyle = 0;
        this.cekStat = 0;
        this.maxspeed = 326;
        this.hp = 320;
        this.weight = 1361;
        this.boost = 0;
        this.rawImage = new ImageIcon("image/supra-stock-garage.png");
        this.raceImage = new ImageIcon("image/supra-stock-race.png");
    }
}
class supramod1 extends suprastock implements Serializable{

    public supramod1() {
        this.maxspeed = maxspeed;
        this.hp = hp;
        this.weight = weight;
        this.boost = boost;
        this.rawImage = new ImageIcon("image/supra-mod1-garage.png");
        this.raceImage = new ImageIcon("image/supra-mod1-race.png");
    }
}

class supramod2 extends supramod1 implements Serializable{
  
    public supramod2() {
        this.maxspeed = maxspeed;
        this.hp = hp;
        this.weight = weight;
        this.boost = boost;
        this.rawImage = new ImageIcon("image/supra-mod2-garage.png");
        this.raceImage = new ImageIcon("image/supra-mod2-race.png");
    }
}
/**
 *
 * @author Albert
 */

public class panelShowroom extends javax.swing.JPanel {

    /**
     * Creates new form panelShowroom
     */
    ArrayList<mobilparent> car = new ArrayList<mobilparent>();
    
    //ArrayList<evostock> evo = new ArrayList<>();
    //ArrayList<nissan370stock> nissan = new ArrayList<>();
    //ArrayList<ft86stock> ft86 = new ArrayList<>();
    //ArrayList<bentleystock> bentley = new ArrayList<>();
    //ArrayList<c63stock> c63 = new ArrayList<>();
    //ArrayList<m4stock> m4 = new ArrayList<>();
    //ArrayList<r8stock> r8 = new ArrayList<>();
    //ArrayList<suprastock> supra = new ArrayList<>();
    ArrayList<Integer> ctrmobil = new ArrayList<Integer>();
    ImageIcon gambar,rawImage;
    JLabel lblUnit;
    int ctr=0,gold;
    public panelShowroom(ArrayList <mobilparent> car,ArrayList<Integer> ctrmobil,int gold) {
        initComponents();
        this.ctrmobil = ctrmobil;
        this.car = car;
        this.gold = gold;
        this.setSize(1500,800);
        
        panelNama.setLocation(500, 600);
        
        jLabel5.setLocation(2000,2000);
        jLabel6.setLocation(2000,2000);
        jLabel7.setLocation(2000,2000);
        jLabel8.setLocation(2000,2000);
        jLabel9.setLocation(2000,2000);
        jLabel10.setLocation(2000,2000);
        jLabel11.setLocation(2000,2000);
        jLabel12.setLocation(2000,2000);
        
        jLabel2.setLocation(30,375);
        jLabel3.setLocation(1400,375);
        jLabel4.setLocation(1400,20);
        jLabel1.setLocation(0,0);
        
        rawImage = new ImageIcon("image/garage.jpg");
        Image img = rawImage.getImage();
        Image newImg = img.getScaledInstance(1500, 800,Image.SCALE_SMOOTH);
        gambar = new ImageIcon(newImg);
        lblUnit = new JLabel(gambar);
        lblUnit.setBounds(0, 0, 1, 1);
        this.add(lblUnit);
        
        rawImage = new ImageIcon("image/garage.jpg");
        img = rawImage.getImage();
        newImg = img.getScaledInstance(1500, 800,Image.SCALE_SMOOTH);
        gambar = new ImageIcon(newImg);
        jLabel2 = new JLabel(gambar);
        jLabel2.setBounds(0, 0, 1, 1);
        this.add(jLabel2);
        
        rawImage = new ImageIcon("image/garage.jpg");
        img = rawImage.getImage();
        newImg = img.getScaledInstance(1500, 800,Image.SCALE_SMOOTH);
        gambar = new ImageIcon(newImg);
        jLabel3 = new JLabel(gambar);
        jLabel3.setBounds(0, 0, 1, 1);
        this.add(jLabel3);
        
        rawImage = new ImageIcon("image/garage.jpg");
        img = rawImage.getImage();
        newImg = img.getScaledInstance(1500, 800,Image.SCALE_SMOOTH);
        gambar = new ImageIcon(newImg);
        jLabel4 = new JLabel(gambar);
        jLabel4.setBounds(0, 0, 1, 1);
        this.add(jLabel4);

        rawImage = new ImageIcon("image/garage.jpg");
        img = rawImage.getImage();
        newImg = img.getScaledInstance(1500, 800,Image.SCALE_SMOOTH);
        gambar = new ImageIcon(newImg);
        jLabel1 = new JLabel(gambar);
        jLabel1.setBounds(0, 0, 1500, 800);
        this.add(jLabel1);
        
        
        jLabelgold.setText("");
        labelgold.setText("");
        buttonBeli.setVisible(false);
        
        lblUnit.setLocation(259, 0);
        buttonBeli.setLocation(980, 515);
        jLabelgold.setLocation(1250, 30);
        labelgold.setLocation(1298, 30);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelNama = new javax.swing.JLabel();
        buttonBeli = new javax.swing.JButton();
        jLabelgold = new javax.swing.JLabel();
        labelgold = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();

        setMaximumSize(new java.awt.Dimension(1500, 800));
        setMinimumSize(new java.awt.Dimension(1500, 800));
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });
        setLayout(null);

        panelNama.setText(" ");
        add(panelNama);
        panelNama.setBounds(743, 91, 53, 101);

        buttonBeli.setText("Buy");
        buttonBeli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBeliActionPerformed(evt);
            }
        });
        add(buttonBeli);
        buttonBeli.setBounds(743, 711, 70, 25);

        jLabelgold.setForeground(new java.awt.Color(255, 255, 255));
        jLabelgold.setText("Gold : ");
        add(jLabelgold);
        jLabelgold.setBounds(32, 754, 62, 29);

        labelgold.setForeground(new java.awt.Color(255, 255, 255));
        labelgold.setText(" ");
        add(labelgold);
        labelgold.setBounds(112, 754, 66, 29);

        jLabel1.setText("jLabel1");
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
        });
        add(jLabel1);
        jLabel1.setBounds(10, 10, 130, 70);

        jLabel2.setText("jLabel2");
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });
        add(jLabel2);
        jLabel2.setBounds(20, 270, 80, 60);

        jLabel3.setText("jLabel3");
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
        });
        add(jLabel3);
        jLabel3.setBounds(850, 290, 110, 70);

        jLabel4.setText("jLabel4");
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
        });
        add(jLabel4);
        jLabel4.setBounds(1030, 20, 90, 70);

        jLabel5.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Top Speed         :");
        add(jLabel5);
        jLabel5.setBounds(207, 653, 180, 28);

        jLabel6.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Hourse Power  :");
        add(jLabel6);
        jLabel6.setBounds(197, 688, 190, 27);

        jLabel7.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Weight               :");
        add(jLabel7);
        jLabel7.setBounds(197, 722, 190, 27);

        jLabel8.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        add(jLabel8);
        jLabel8.setBounds(399, 653, 121, 28);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        add(jLabel9);
        jLabel9.setBounds(399, 688, 131, 27);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        add(jLabel10);
        jLabel10.setBounds(399, 722, 120, 31);

        jLabel11.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("PriceList ");
        add(jLabel11);
        jLabel11.setBounds(198, 550, 110, 40);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        add(jLabel12);
        jLabel12.setBounds(320, 550, 110, 40);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonBeliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBeliActionPerformed
        // TODO add your handling code here:
        Boolean cek = true;
        for(int i=0;i<ctrmobil.size();i++){
            if(ctrmobil.get(i)==ctr)cek = false;
        }
        if(cek==true){
            ctrmobil.add(ctr);
            if(ctr==0 && gold>=40000){
                mobilparent mp1 = new ft86stock();
                car.add(mp1);
                gold-=40000;
            }
            else if(ctr==1 && gold>=50000){
                mobilparent mp1 = new nissan370stock();
                car.add(mp1);
                gold-=50000;
            }
            else if(ctr==2 && gold>=100000){
                mobilparent mp1 = new bentleystock();
                car.add(mp1);
                gold-=100000;
            }
            else if(ctr==3 && gold>=45000){
                mobilparent mp1 = new evostock();
                car.add(mp1);
                gold-=45000;
            }
            else if(ctr==4 && gold>=80000){
                mobilparent mp1 = new m4stock();
                car.add(mp1);
                gold-=80000;
            }
            else if(ctr==5 && gold>=85000){
                mobilparent mp1 = new r8stock();
                car.add(mp1);
                gold-=85000;
            }
            else if(ctr==6 && gold>=60000){
                mobilparent mp1 = new suprastock();
                car.add(mp1);
                gold-=60000;
            }
            else if(ctr==7 && gold>=90000){
                mobilparent mp1 = new c63stock();
                car.add(mp1);
                gold-=90000;
                
            }
            car.get(car.size()-1).setUse(1);
            JOptionPane.showMessageDialog(null, "Item Purchased!!!");
            for(int i=0;i<car.size()-1;i++){
                car.get(i).setUse(0);
            }
            for(int i=0;i<car.size();i++){
                System.out.println(car.get(i).getUse());
            }
            System.out.println("======================");
        }
        else JOptionPane.showMessageDialog(null, "Mobil sudah dibeli");
        
        labelgold.setText(gold+"");
    }//GEN-LAST:event_buttonBeliActionPerformed

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, evt.getX() +"\n"+ evt.getY() + "");
    }//GEN-LAST:event_formMouseClicked

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        // TODO add your handling code here:
        jLabel8.setText("200");
        jLabel9.setText("200");
        jLabel10.setText("1298");
        jLabel12.setText("$  40000");
        
        jLabel5.setLocation(900, 400);
        jLabel6.setLocation(902, 430);
        jLabel7.setLocation(903, 460);
        jLabel8.setLocation(1110, 400);
        jLabel9.setLocation(1110, 430);
        jLabel10.setLocation(1110, 460);
        jLabel11.setLocation(915,355);
        jLabel12.setLocation(1045,352);
        
        jLabelgold.setText("Gold : ");
        labelgold.setText(gold+"");
        buttonBeli.setVisible(true);
        
        ImageIcon rawImage1 = new ImageIcon("image/86-stock-garage.png");
        Image img1 = rawImage1.getImage();
        Image newImg1 = img1.getScaledInstance(1000, 500,Image.SCALE_SMOOTH);
        ImageIcon gambar1 = new ImageIcon(newImg1);
        lblUnit.setBounds(80, 225, 1000, 500);
        lblUnit.setIcon(gambar1);
        
        ImageIcon rawImage2 = new ImageIcon("image/panahki.png");
        Image img2 = rawImage2.getImage();
        Image newImg2 = img2.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        ImageIcon gambar2 = new ImageIcon(newImg2);
        jLabel2.setBounds(30, 375, 50, 50);
        jLabel2.setIcon(gambar2);
        
        rawImage2 = new ImageIcon("image/panahk.png");
        img2 = rawImage2.getImage();
        newImg2 = img2.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        gambar2 = new ImageIcon(newImg2);
        jLabel3.setBounds(1400, 375, 50, 50);
        jLabel3.setIcon(gambar2);
        
        rawImage2 = new ImageIcon("image/back.png");
        img2 = rawImage2.getImage();
        newImg2 = img2.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        gambar2 = new ImageIcon(newImg2);
        jLabel4.setBounds(1400, 20, 50, 50);
        jLabel4.setIcon(gambar2);
        
        rawImage = new ImageIcon("image/car bg.jpg");
        Image img = rawImage.getImage();
        Image newImg = img.getScaledInstance(1500, 800,Image.SCALE_SMOOTH);
        gambar = new ImageIcon(newImg);
        jLabel1.setIcon(gambar);
    }//GEN-LAST:event_jLabel1MouseClicked

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        // TODO add your handling code here:
        //prev
        ctr--;
        if(ctr<0)ctr=7;
        
        if(ctr==0){
            rawImage = new ImageIcon("image/86-stock-garage.png");
            jLabel8.setText("200");
            jLabel9.setText("200");
            jLabel10.setText("1298");
            jLabel12.setText("$  40000");
        }
        else if(ctr==1){
            rawImage = new ImageIcon("image/370z-stock-garage.png");
            jLabel8.setText("249");
            jLabel9.setText("350");
            jLabel10.setText("1466");
            jLabel12.setText("$  50000");
        }
        else if(ctr==2){
            rawImage = new ImageIcon("image/bentley-stock-garage.png");
            jLabel8.setText("345");
            jLabel9.setText("700");
            jLabel10.setText("2320");
            jLabel12.setText("$ 100000");
        }
        else if(ctr==3){
            rawImage = new ImageIcon("image/evo-stock-garage.png");
            jLabel8.setText("250");
            jLabel9.setText("193");
            jLabel10.setText("1482");
            jLabel12.setText("$  45000");
        }
        else if(ctr==4){
            rawImage = new ImageIcon("image/m4-stock-garage.png");
            jLabel8.setText("305");
            jLabel9.setText("425");
            jLabel10.setText("1572");
            jLabel12.setText("$  80000");
        }
        else if(ctr==5){
            rawImage = new ImageIcon("image/r8-stock-garage.png");
            jLabel8.setText("316");
            jLabel9.setText("540");
            jLabel10.setText("1525");
            jLabel12.setText("$  85000");
        }
        else if(ctr==6){
            rawImage = new ImageIcon("image/supra-stock-garage.png");
            jLabel8.setText("326");
            jLabel9.setText("320");
            jLabel10.setText("1361");
            jLabel12.setText("$  60000");
        }
        else if(ctr==7){
            rawImage = new ImageIcon("image/c63-stock-garage.png");
            jLabel8.setText("320");
            jLabel9.setText("469");
            jLabel10.setText("1730");
            jLabel12.setText("$  90000");
        }
        Image img = rawImage.getImage();
        Image newImg = img.getScaledInstance(1000, 500,Image.SCALE_SMOOTH);
        gambar = new ImageIcon(newImg);
        lblUnit.setIcon(gambar);
    }//GEN-LAST:event_jLabel2MouseClicked

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked
        // TODO add your handling code here:
        //next
        ctr++;
        if(ctr>7)ctr = 0;
        if(ctr==0){
            rawImage = new ImageIcon("image/86-stock-garage.png");
            jLabel8.setText("200");
            jLabel9.setText("200");
            jLabel10.setText("1298");
            jLabel12.setText("$  40000");
        }
        else if(ctr==1){
            rawImage = new ImageIcon("image/370z-stock-garage.png");
            jLabel8.setText("249");
            jLabel9.setText("350");
            jLabel10.setText("1466");
            jLabel12.setText("$  50000");
        }
        else if(ctr==2){
            rawImage = new ImageIcon("image/bentley-stock-garage.png");
            jLabel8.setText("345");
            jLabel9.setText("700");
            jLabel10.setText("2320");
            jLabel12.setText("$ 100000");
        }
        else if(ctr==3){
            rawImage = new ImageIcon("image/evo-stock-garage.png");
            jLabel8.setText("250");
            jLabel9.setText("193");
            jLabel10.setText("1482");
            jLabel12.setText("$  45000");
        }
        else if(ctr==4){
            rawImage = new ImageIcon("image/m4-stock-garage.png");
            jLabel8.setText("305");
            jLabel9.setText("425");
            jLabel10.setText("1572");
            jLabel12.setText("$  80000");
        }
        else if(ctr==5){
            rawImage = new ImageIcon("image/r8-stock-garage.png");
            jLabel8.setText("316");
            jLabel9.setText("540");
            jLabel10.setText("1525");
            jLabel12.setText("$  85000");
        }
        else if(ctr==6){
            rawImage = new ImageIcon("image/supra-stock-garage.png");
            jLabel8.setText("326");
            jLabel9.setText("320");
            jLabel10.setText("1361");
            jLabel12.setText("$  60000");
        }
        else if(ctr==7){
            rawImage = new ImageIcon("image/c63-stock-garage.png");
            jLabel8.setText("320");
            jLabel9.setText("469");
            jLabel10.setText("1730");
            jLabel12.setText("$  90000");
        }
        Image img = rawImage.getImage();
        Image newImg = img.getScaledInstance(1000, 500,Image.SCALE_SMOOTH);
        gambar = new ImageIcon(newImg);
        lblUnit.setIcon(gambar);
    }//GEN-LAST:event_jLabel3MouseClicked

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        // TODO add your handling code here:
        //back
        frame1 f1 = (frame1) this.getParent().getParent().getParent().getParent();
        f1.menuutama(gold);
    }//GEN-LAST:event_jLabel4MouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonBeli;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelgold;
    private javax.swing.JLabel labelgold;
    private javax.swing.JLabel panelNama;
    // End of variables declaration//GEN-END:variables
}
