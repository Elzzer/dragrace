/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dragrace;

import java.awt.Image;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Albert
 */
public class panelStyle extends javax.swing.JPanel {

    /**
     * Creates new form panelStyle
     */
    int ctr;
    int gold;
    ArrayList <mobilparent> car = new ArrayList<>();
    ArrayList <Integer> ctrmobil = new ArrayList<>();
    ImageIcon rawImage;
    public panelStyle(int ctr, ArrayList <mobilparent> car, ArrayList<Integer> ctrmobil,int gold) {
        initComponents();
        this.setSize(1500,800);
        this.car = car;
        this.ctr = ctr;
        this.ctrmobil = ctrmobil;
        this.gold = gold;
        jLabel2.setText(gold+"");
        
        jButton1.setLocation(695,378);
        jButton2.setLocation(1350,20);
        jLabel1.setLocation(1250, 30);
        jLabel2.setLocation(1298, 30);
        
        jLabel11.setLocation(640,442);
        jLabel12.setLocation(770,439);
        
        if(car.size()!=0){
            if(car.get(ctr).getCekStyle()==0){
                jLabel12.setText("$  20000");
            }
            else if(car.get(ctr).getCekStyle()==1){
                jLabel12.setText("$  35000");
            } 
            
            if(ctrmobil.get(ctr)==0){
                if(car.get(ctr).getCekStyle()==0){
                    ft86mod1 temp = new ft86mod1();
                    rawImage = temp.getRawImage();
                }
                else if(car.get(ctr).getCekStyle()==1){
                    ft86mod2 temp = new ft86mod2();
                    rawImage = temp.getRawImage();
                }
            }
            else if(ctrmobil.get(ctr)==1){
                if(car.get(ctr).getCekStyle()==0){
                    nissan370mod1 temp = new nissan370mod1();
                    rawImage = temp.getRawImage();
                }
                else if(car.get(ctr).getCekStyle()==1){
                    nissan370mod2 temp = new nissan370mod2();
                    rawImage = temp.getRawImage();
                }
            }
            else if(ctrmobil.get(ctr)==2){
                if(car.get(ctr).getCekStyle()==0){
                    bentleymod1 temp = new bentleymod1();
                    rawImage = temp.getRawImage();
                }
                else if(car.get(ctr).getCekStyle()==1){
                    bentleymod2 temp = new bentleymod2();
                    rawImage = temp.getRawImage();
                }
            }
            else if(ctrmobil.get(ctr)==3){
                if(car.get(ctr).getCekStyle()==0){
                    evomod1 temp = new evomod1();
                    rawImage = temp.getRawImage();
                }
                else if(car.get(ctr).getCekStyle()==1){
                    evomod2 temp = new evomod2();
                    rawImage = temp.getRawImage();
                }
            }
            else if(ctrmobil.get(ctr)==4){
                if(car.get(ctr).getCekStyle()==0){
                    m4mod1 temp = new m4mod1();
                    rawImage = temp.getRawImage();
                }
                else if(car.get(ctr).getCekStyle()==1){
                    m4mod2 temp = new m4mod2();
                    rawImage = temp.getRawImage();
                }
            }
            else if(ctrmobil.get(ctr)==5){
                if(car.get(ctr).getCekStyle()==0){
                    r8mod1 temp = new r8mod1();
                    rawImage = temp.getRawImage();
                }
                else if(car.get(ctr).getCekStyle()==1){
                    r8mod2 temp = new r8mod2();
                    rawImage = temp.getRawImage();
                }
            }
            else if(ctrmobil.get(ctr)==6){
                if(car.get(ctr).getCekStyle()==0){
                    supramod1 temp = new supramod1();
                    rawImage = temp.getRawImage();
                }
                else if(car.get(ctr).getCekStyle()==1){
                    supramod2 temp = new supramod2();
                    rawImage = temp.getRawImage();
                }
            }
            else if(ctrmobil.get(ctr)==7){
                if(car.get(ctr).getCekStyle()==0){
                    c63mod1 temp = new c63mod1();
                    rawImage = temp.getRawImage();
                }
                else if(car.get(ctr).getCekStyle()==1){
                    c63mod2 temp = new c63mod2();
                    rawImage = temp.getRawImage();
                }
            }
            
            Image img = rawImage.getImage();
            Image newImg = img.getScaledInstance(750, 400,Image.SCALE_SMOOTH);
            ImageIcon gambar = new ImageIcon(newImg);
            lblUnit = new JLabel(gambar);
            lblUnit.setBounds(750, 330, 750, 400);
            this.add(lblUnit);
            
            rawImage = car.get(ctr).getRawImage();
            img = rawImage.getImage();
            newImg = img.getScaledInstance(750, 400,Image.SCALE_SMOOTH);
            gambar = new ImageIcon(newImg);
            lblUnitAwal = new JLabel(gambar);
            lblUnitAwal.setBounds(0, 330, 750, 400);
            this.add(lblUnitAwal);

            rawImage = new ImageIcon("image/upgrade arrow.png");
            img = rawImage.getImage();
            newImg = img.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
            gambar = new ImageIcon(newImg);
            jLabel4 = new JLabel(gambar);
            jLabel4.setBounds(708, 545, 50, 50);
            this.add(jLabel4);

            rawImage = new ImageIcon("image/car bg.jpg");
            img = rawImage.getImage();
            newImg = img.getScaledInstance(1500, 800,Image.SCALE_SMOOTH);
            gambar = new ImageIcon(newImg);
            jLabel3 = new JLabel(gambar);
            jLabel3.setBounds(0, 0, 1500, 800);
            this.add(jLabel3);
        }
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblUnit = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblUnitAwal = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();

        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });
        setLayout(null);
        add(lblUnit);
        lblUnit.setBounds(467, 28, 349, 390);

        jButton1.setText("Upgrade");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1);
        jButton1.setBounds(247, 485, 111, 43);

        jButton2.setText("Cancel");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        add(jButton2);
        jButton2.setBounds(497, 485, 99, 43);

        jLabel1.setText("Gold : ");
        add(jLabel1);
        jLabel1.setBounds(166, 554, 60, 26);

        jLabel2.setText(" ");
        add(jLabel2);
        jLabel2.setBounds(233, 554, 51, 26);

        lblUnitAwal.setText("jLabel");
        add(lblUnitAwal);
        lblUnitAwal.setBounds(0, 115, 386, 255);

        jLabel3.setText(" ");
        add(jLabel3);
        jLabel3.setBounds(328, 431, 102, 47);

        jLabel4.setText("jLabel4");
        add(jLabel4);
        jLabel4.setBounds(408, 205, 41, 74);

        jLabel11.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("PriceList ");
        add(jLabel11);
        jLabel11.setBounds(198, 550, 110, 40);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        add(jLabel12);
        jLabel12.setBounds(320, 550, 110, 40);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        Boolean cekbeli = true;
        if(car.get(ctr).getCekStyle()==0 && gold>=20000){
            cekbeli = true;
            gold-=20000;
        }
        else if(car.get(ctr).getCekStyle()==1 && gold>=35000){
            cekbeli = true;
            gold-=35000;
        }
        else {
            cekbeli = false;
            JOptionPane.showMessageDialog(null, "Uang Tidak Cukup!!!");
        }
        
        if(cekbeli == true){
            int a = car.get(ctr).getUse();
            if(ctrmobil.get(ctr)==0){
                if(car.get(ctr).getCekStyle()==0){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new ft86mod1());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
                else if(car.get(ctr).getCekStyle()==1){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new ft86mod2());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
            }
            else if(ctrmobil.get(ctr)==1){
                if(car.get(ctr).getCekStyle()==0){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new nissan370mod1());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
                else if(car.get(ctr).getCekStyle()==1){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new nissan370mod2());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
            }
            else if(ctrmobil.get(ctr)==2){
                if(car.get(ctr).getCekStyle()==0){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new bentleymod1());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
                else if(car.get(ctr).getCekStyle()==1){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new bentleymod2());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
            }
            else if(ctrmobil.get(ctr)==3){
                if(car.get(ctr).getCekStyle()==0){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new evomod1());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
                else if(car.get(ctr).getCekStyle()==1){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new evomod2());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
            }
            else if(ctrmobil.get(ctr)==4){
                if(car.get(ctr).getCekStyle()==0){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new m4mod1());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
                else if(car.get(ctr).getCekStyle()==1){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new m4mod2());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
            }
            else if(ctrmobil.get(ctr)==5){
                if(car.get(ctr).getCekStyle()==0){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new r8mod1());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
                else if(car.get(ctr).getCekStyle()==1){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new r8mod2());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
            }
            else if(ctrmobil.get(ctr)==6){
                if(car.get(ctr).getCekStyle()==0){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new supramod1());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
                else if(car.get(ctr).getCekStyle()==1){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new supramod2());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
            }
            else if(ctrmobil.get(ctr)==7){
                if(car.get(ctr).getCekStyle()==0){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new c63mod1());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
                else if(car.get(ctr).getCekStyle()==1){
                    int tcekstyle = car.get(ctr).getCekStyle();
                    car.set(ctr, new c63mod2());
                    car.get(ctr).setCekStyle(tcekstyle+1);
                }
            }
            car.get(ctr).setBoost(car.get(ctr).getBoost() + 1);
            car.get(ctr).setUse(a);
        }
        
        frame1 f = (frame1) this.getParent().getParent().getParent().getParent();
        f.tuner(gold);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        frame1 f = (frame1) this.getParent().getParent().getParent().getParent();
        f.tuner(gold);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, evt.getX() +"\n"+ evt.getY() + "");
    }//GEN-LAST:event_formMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel lblUnit;
    private javax.swing.JLabel lblUnitAwal;
    // End of variables declaration//GEN-END:variables
}
