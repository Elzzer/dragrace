/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dragrace;

import java.io.Serializable;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author Albert
 */
public class mobilparent implements Serializable{
    public int maxspeed;
    public int hp;
    public int weight;
    public int boost;
    JLabel lblunit;
    ImageIcon rawImage;
    ImageIcon raceImage;
    public int use;
    public int cekStyle;
    public int cekStat;

    public int getMaxspeed() {
        return maxspeed;
    }

    public void setMaxspeed(int maxspeed) {
        this.maxspeed = maxspeed;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getBoost() {
        return boost;
    }

    public void setBoost(int boost) {
        this.boost = boost;
    }

    public JLabel getLblunit() {
        return lblunit;
    }

    public void setLblunit(JLabel lblunit) {
        this.lblunit = lblunit;
    }

    public ImageIcon getRawImage() {
        return rawImage;
    }

    public void setRawImage(ImageIcon rawImage) {
        this.rawImage = rawImage;
    }

    public int getUse() {
        return use;
    }

    public void setUse(int use) {
        this.use = use;
    }

    public int getCekStyle() {
        return cekStyle;
    }

    public void setCekStyle(int cekStyle) {
        this.cekStyle = cekStyle;
    }

    public int getCekStat() {
        return cekStat;
    }

    public void setCekStat(int cekStat) {
        this.cekStat = cekStat;
    }

    public ImageIcon getRaceImage() {
        return raceImage;
    }

    public void setRaceImage(ImageIcon raceImage) {
        this.raceImage = raceImage;
    }
    
}
