/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dragrace;

import java.awt.Image;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Albert
 */
public class panelLoadingRace extends javax.swing.JPanel {

    /**
     * Creates new form panelLoadingRace
     */
    int gold,rand,miles;
    ArrayList <mobilparent> car = new ArrayList<>();
    ImageIcon gambarE = null, rawImage = null,gambarER = null;
    
    public panelLoadingRace(ArrayList <mobilparent> car,int gold,int miles) {
        initComponents();
        this.setSize(1500,800);
        this.car = car;
        this.gold = gold;
        this.miles = miles;
        jLabel3.setLocation(750,400);
        
        Boolean cek = true;
        for(int i=0;i<car.size();i++){
            if(car.get(i).getUse()==1){
                cek = false;
            }
        }
        
        if(cek==false){
            for(int i=0;i<car.size();i++){
                if(car.get(i).getUse()==1){
                    rawImage = car.get(i).getRawImage();
                    rand = (int)(Math.random()*8);
                    System.out.println("random : " + rand);
                    if(car.get(i).getCekStyle()==0){
                        if(rand==0){
                            gambarE = new ImageIcon("image/86-stock-garage.png");
                            gambarER = new ImageIcon("image/86-stock-race.png");
                        }
                        else if(rand==1){
                            gambarE = new ImageIcon("image/370z-stock-garage.png");
                            gambarER = new ImageIcon("image/370z-stock-race.png");
                        }
                        else if(rand==2){
                            gambarE = new ImageIcon("image/bentley-stock-garage.png");
                            gambarER = new ImageIcon("image/bentley-stock-race.png");
                        }
                        else if(rand==3){
                            gambarE = new ImageIcon("image/evo-stock-garage.png");
                            gambarER = new ImageIcon("image/evo-stock-race.png");
                        }
                        else if(rand==4){
                            gambarE = new ImageIcon("image/m4-stock-garage.png");
                            gambarER = new ImageIcon("image/m4-stock-race.png");
                        }
                        else if(rand==5){
                            gambarE = new ImageIcon("image/r8-stock-garage.png");
                            gambarER = new ImageIcon("image/r8-stock-race.png");
                        }
                        else if(rand==6){
                            gambarE = new ImageIcon("image/supra-stock-garage.png");
                            gambarER = new ImageIcon("image/supra-stock-race.png");
                        }
                        else if(rand==7){
                            gambarE = new ImageIcon("image/c63-stock-garage.png");
                            gambarER = new ImageIcon("image/c63-stock-race.png");
                        }
                    }
                    else if(car.get(i).getCekStyle()==1){
                        if(rand==0){
                            gambarE = new ImageIcon("image/86-mod1-garage.png");
                            gambarER = new ImageIcon("image/86-mod1-race.png");
                        }
                        else if(rand==1){
                            gambarE = new ImageIcon("image/370z-mod1-garage.png");
                            gambarER = new ImageIcon("image/370z-mod1-race.png");
                        }
                        else if(rand==2){
                            gambarE = new ImageIcon("image/bentley-mod-garage.png");
                            gambarER = new ImageIcon("image/bentley-mod-race.png");
                        }
                        else if(rand==3){
                            gambarE = new ImageIcon("image/evo-mod1-garage.png");
                            gambarER = new ImageIcon("image/evo-mod1-race.png");
                        }
                        else if(rand==4){
                            gambarE = new ImageIcon("image/m4-mod1-garage.png");
                            gambarER = new ImageIcon("image/m4-mod1-race.png");
                        }
                        else if(rand==5){
                            gambarE = new ImageIcon("image/r8-mod1-garage.png");
                            gambarER = new ImageIcon("image/r8-mod1-race.png");
                        }
                        else if(rand==6){
                            gambarE = new ImageIcon("image/supra-mod1-garage.png");
                            gambarER = new ImageIcon("image/supra-mod1-race.png");
                        }
                        else if(rand==7){
                            gambarE = new ImageIcon("image/c63-mod1-garage.png");
                            gambarER = new ImageIcon("image/c63-mod1-race.png");
                        }
                    }
                    else if(car.get(i).getCekStyle()==2){
                        if(rand==0){
                            gambarE = new ImageIcon("image/86-mod2-garage.png");
                            gambarER = new ImageIcon("image/86-mod2-race.png");
                        }
                        else if(rand==1){
                            gambarE = new ImageIcon("image/370z-mod2-garage.png");
                            gambarER = new ImageIcon("image/370z-mod2-race.png");
                        }
                        else if(rand==2){
                            gambarE = new ImageIcon("image/bentley-mod2-garage.png");
                            gambarER = new ImageIcon("image/bentley-mod2-race.png");
                        }
                        else if(rand==3){
                            gambarE = new ImageIcon("image/evo-mod2-garage.png");
                            gambarER = new ImageIcon("image/evo-mod2-race.png");
                        }
                        else if(rand==4){
                            gambarE = new ImageIcon("image/m4-mod2-garage.png");
                            gambarER = new ImageIcon("image/m4-mod2-race.png");
                        }
                        else if(rand==5){
                            gambarE = new ImageIcon("image/r8-mod2-garage.png");
                            gambarER = new ImageIcon("image/r8-mod2-race.png");
                        }
                        else if(rand==6){
                            gambarE = new ImageIcon("image/supra-mod2-garage.png");
                            gambarER = new ImageIcon("image/supra-mod2-race.png");
                        }
                        else if(rand==7){
                            gambarE = new ImageIcon("image/c63-mod2-garage.png");
                            gambarER = new ImageIcon("image/c63-mod2-race.png");
                        }
                    }
                }
            }
            
            Image img = gambarE.getImage();
            Image newImg = img.getScaledInstance(1000, 500,Image.SCALE_SMOOTH);
            ImageIcon gambar = new ImageIcon(newImg);
            jLabel2 = new JLabel(gambar);
            jLabel2.setBounds(750, 270, 750, 500);
            this.add(jLabel2);
            
            Image img1 = rawImage.getImage();
            Image newImg1 = img1.getScaledInstance(1000, 500,Image.SCALE_SMOOTH);
            ImageIcon gambar1 = new ImageIcon(newImg1);
            jLabel1 = new JLabel(gambar1);
            jLabel1.setBounds(0, 0, 750, 500);
            this.add(jLabel1);
            
            rawImage = new ImageIcon("image/versus.jpg");
            img1 = rawImage.getImage();
            newImg1 = img1.getScaledInstance(1500, 800,Image.SCALE_SMOOTH);
            gambar1 = new ImageIcon(newImg1);
            jLabel1 = new JLabel(gambar1);
            jLabel1.setBounds(0, 0, 1500, 800);
            this.add(jLabel1);
            
        }
        else if(cek==true && gold>0){
            JOptionPane.showMessageDialog(null, "Belum ada Mobil!!!");
            frame1 f = (frame1) this.getParent().getParent().getParent().getParent();
            f.menuutama(gold);
        }
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setLayout(null);

        jLabel1.setText(" ");
        add(jLabel1);
        jLabel1.setBounds(85, 101, 124, 123);

        jLabel2.setText(" ");
        add(jLabel2);
        jLabel2.setBounds(638, 101, 127, 123);

        jLabel3.setText("ajkdshflakjshdflajhd");
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
        });
        add(jLabel3);
        jLabel3.setBounds(372, 198, 113, 93);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked
        // TODO add your handling code here:
        frame1 f = (frame1) this.getParent().getParent().getParent().getParent();
        int xPlayer = 0,xEnemy = 0;
        f.Race(gold,gambarER,miles,xPlayer,xEnemy);
    }//GEN-LAST:event_jLabel3MouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    // End of variables declaration//GEN-END:variables
}
