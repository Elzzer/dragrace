/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dragrace;

import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author Ferdinan
 */
public class frame1 extends javax.swing.JFrame implements Runnable{

    /**
     * Creates new form frame1
     */
    ArrayList<mobilparent> car = new ArrayList<mobilparent>();
    ArrayList<Integer> ctrmobil = new ArrayList<Integer>();
    ImageIcon gambarER = null;
    int ctr = 0;
    int gold = 0,miles = 0;
    int cekgerak = 0,ctrthread,rand;
    int percepatan = 0,rpm = 0;
    int nitro = 0;
    int temp = 0;
    int xPlayer=0,xEnemy=0;
    
    panel1 p1=new panel1();
    panelMenu p2 = new panelMenu(gold,car,ctrmobil);
    panelMenuUtama p3 = new panelMenuUtama(gold,car,ctrmobil);
    panelGarage p4 = new panelGarage(car,gold);
    panelShowroom p5 = new panelShowroom(car,ctrmobil,gold);
    panelTuner p6 = new panelTuner(car,gold);
    panelUpgrade p7 = new panelUpgrade(ctr,car,gold);
    panelStyle p8 = new panelStyle(ctr,car,ctrmobil,gold);
    panelLoadingRace p9 = new panelLoadingRace(car,gold,miles);
    panelRace p10 = new panelRace(car,gold,gambarER,miles,xPlayer,xEnemy);
    Boolean jalan = false;
    
    int eSpeed , pSpeed;
    
    public frame1() {
        initComponents();
        this.setFocusable(true);
        this.setSize(1500, 800);
        p1 = new panel1();
        this.add(p1);
        p1.setVisible(true);
        p2.setVisible(false);
        p3.setVisible(false);
        p4.setVisible(false);
        p5.setVisible(false);
        p6.setVisible(false);
        p7.setVisible(false);
        p8.setVisible(false);
        p9.setVisible(false);
        p10.setVisible(false);
    }

    public void login(int gold){
        p2 = new panelMenu(gold,car,ctrmobil);
        this.add(p2);
        p2.setVisible(true);
        p1.setVisible(false);
        p3.setVisible(false);
        p4.setVisible(false);
        p5.setVisible(false);
        p6.setVisible(false);
        p7.setVisible(false);
        p8.setVisible(false);
        p9.setVisible(false);
        p10.setVisible(false);
    }
    public void menuutama(int gold){
        p3 = new panelMenuUtama(gold,car,ctrmobil);
        this.add(p3);
        p3.setVisible(true);
        p1.setVisible(false);
        p2.setVisible(false);
        p4.setVisible(false);
        p5.setVisible(false);
        p6.setVisible(false);
        p7.setVisible(false);
        p8.setVisible(false);
        p9.setVisible(false);
        p10.setVisible(false);
    }
    public void garage(int gold){
        p4 = new panelGarage(car,gold);
        this.add(p4);
        p4.setVisible(true);
        p1.setVisible(false);
        p2.setVisible(false);
        p3.setVisible(false);
        p5.setVisible(false);
        p6.setVisible(false);
        p7.setVisible(false);
        p8.setVisible(false);
        p9.setVisible(false);
        p10.setVisible(false);
    }
    
    public void Showroom(int gold){
        p5 = new panelShowroom(car,ctrmobil,gold);
        this.add(p5);
        p4.setVisible(false);
        p1.setVisible(false);
        p2.setVisible(false);
        p3.setVisible(false);
        p5.setVisible(true);
        p6.setVisible(false);
        p7.setVisible(false);
        p8.setVisible(false);
        p9.setVisible(false);
        p10.setVisible(false);
    }
    
    public void tuner(int gold){
        p6 = new panelTuner(car,gold);
        this.add(p6);
        p4.setVisible(false);
        p1.setVisible(false);
        p2.setVisible(false);
        p3.setVisible(false);
        p5.setVisible(false);
        p6.setVisible(true);
        p7.setVisible(false);
        p8.setVisible(false);
        p9.setVisible(false);
        p10.setVisible(false);
    }
    
    public void upgradestats(int ctr,int gold){
        p7 = new panelUpgrade(ctr,car,gold);
        this.add(p7);
        p4.setVisible(false);
        p1.setVisible(false);
        p2.setVisible(false);
        p3.setVisible(false);
        p5.setVisible(false);
        p6.setVisible(false);
        p7.setVisible(true);
        p8.setVisible(false);
        p9.setVisible(false);
        p10.setVisible(false);
    }
    
    public void Style(int ctr,int gold){
        p8 = new panelStyle(ctr,car,ctrmobil,gold);
        this.add(p8);
        p4.setVisible(false);
        p1.setVisible(false);
        p2.setVisible(false);
        p3.setVisible(false);
        p5.setVisible(false);
        p6.setVisible(false);
        p7.setVisible(false);
        p8.setVisible(true);
        p9.setVisible(false);
        p10.setVisible(false);
    }
    
    public void LoadingRace(int gold,int miles){
        p9 = new panelLoadingRace(car,gold,miles);
        this.add(p9);
        p4.setVisible(false);
        p1.setVisible(false);
        p2.setVisible(false);
        p3.setVisible(false);
        p5.setVisible(false);
        p6.setVisible(false);
        p7.setVisible(false);
        p8.setVisible(false);
        p9.setVisible(true);
        p10.setVisible(false);
    }
    
    public void Race(int gold,ImageIcon gambarER,int miles,int xPlayer, int xEnemy){
        this.gold = gold;
        
        for(int i=0;i<car.size();i++){
            if(car.get(i).getUse()==1){
                System.out.println(car.get(i).getMaxspeed());
                pSpeed = car.get(i).getMaxspeed();
                nitro = car.get(i).getBoost();
                System.out.println("boost : " + nitro);
            }
        }
        
        jalan = false;
        cekgerak = 0;
        ctrthread = 500;
        rpm = 0;temp = 1;
        
        p10 = new panelRace(car,gold,gambarER,miles,xPlayer,xEnemy);
        this.add(p10);
        p4.setVisible(false);
        p1.setVisible(false);
        p2.setVisible(false);
        p3.setVisible(false);
        p5.setVisible(false);
        p6.setVisible(false);
        p7.setVisible(false);
        p8.setVisible(false);
        p9.setVisible(false);
        p10.setVisible(true);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        // TODO add your handling code here:
        char t = evt.getKeyChar();
        if(t=='d'){
            cekgerak = 1;
            if(jalan==false){
                jalan = true;
                p10.player.run();
            }
            
            if(p10.player.lblUnit.getX()>=1450){
                p10.player.stop();
                p10.enemy.stop();
                JOptionPane.showMessageDialog(null, "Anda Menang!!!");
                menuutama(gold+10000);
            }
            else if(p10.enemy.lblUnit.getX() >= 1450){
                p10.player.stop();
                p10.enemy.stop();
                JOptionPane.showMessageDialog(null, "Anda Kalah!!!");
                menuutama(gold);
            }
            p10.rpmlabel.setText(rpm + "");
        }
        else if(t=='a'){
            cekgerak = 2;
            if(jalan==false){
                jalan = true;
                //System.out.println("berenti!!!!");
                p10.player.run();
            }
            
            if(p10.player.lblUnit.getX()>=1450){
                p10.player.stop();
                p10.enemy.stop();
                JOptionPane.showMessageDialog(null, "Anda Menang!!!");
                menuutama(gold+10000);
            }
            else if(p10.enemy.lblUnit.getX() >= 1450){
                p10.player.stop();
                p10.enemy.stop();
                JOptionPane.showMessageDialog(null, "Anda Kalah!!!");
                menuutama(gold);
            }
            p10.rpmlabel.setText(rpm + "");
        }
        else if(t==32){
            System.out.println("masuk nitro");
            if(nitro>0){
                System.out.println("nitro!!!");
                cekgerak = 3;
                if(jalan==false){
                    jalan = true;
                    p10.player.run();
                }
                nitro--;
            }
            else{
                cekgerak = 0;
                if(jalan==false){
                    jalan = true;
                    //System.out.println("berenti!!!!");
                    p10.player.run();

                }
            }
            if(p10.player.lblUnit.getX()>=1450){
                p10.player.stop();
                p10.enemy.stop();
                JOptionPane.showMessageDialog(null, "Anda Menang!!!");
                menuutama(gold+10000);
            }
            else if(p10.enemy.lblUnit.getX() >= 1450){
                p10.player.stop();
                p10.enemy.stop();
                JOptionPane.showMessageDialog(null, "Anda Kalah!!!");
                menuutama(gold);
            }
            p10.rpmlabel.setText(rpm + "");
        }
    }//GEN-LAST:event_formKeyPressed

    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
        // TODO add your handling code here:
        char t = evt.getKeyChar();
        if(t=='d'){
            cekgerak = 0;
            if(jalan==false){
                jalan = true;
                //System.out.println("berenti!!!!");
                p10.player.run();
                
            }
            
            if(p10.player.lblUnit.getX()>=1450){
                p10.player.stop();
                p10.enemy.stop();
                JOptionPane.showMessageDialog(null, "Anda Menang!!!");
                menuutama(gold+10000);
            }
            else if(p10.enemy.lblUnit.getX() >= 1450){
                p10.player.stop();
                p10.enemy.stop();
                JOptionPane.showMessageDialog(null, "Anda Kalah!!!");
                menuutama(gold);
            }
            p10.rpmlabel.setText(rpm + "");
        }
        else if(t=='a'){
            cekgerak = 0;
            if(jalan==false){
                jalan = true;
                //System.out.println("berenti!!!!");
                p10.player.run();
                
            }
            
            if(p10.player.lblUnit.getX()>=1450){
                p10.player.stop();
                p10.enemy.stop();
                JOptionPane.showMessageDialog(null, "Anda Menang!!!");
                menuutama(gold+10000);
            }
            else if(p10.enemy.lblUnit.getX() >= 1450){
                p10.player.stop();
                p10.enemy.stop();
                JOptionPane.showMessageDialog(null, "Anda Kalah!!!");
                menuutama(gold);
            }
            p10.rpmlabel.setText(rpm + "");
        }
        else if(t==32){
            cekgerak = 0;
            if(jalan==false){
                jalan = true;
                //System.out.println("berenti!!!!");
                p10.player.run();
                
            }
            
            if(p10.player.lblUnit.getX()>=1450){
                p10.player.stop();
                p10.enemy.stop();
                JOptionPane.showMessageDialog(null, "Anda Menang!!!");
                menuutama(gold+10000);
            }
            else if(p10.enemy.lblUnit.getX() >= 1450){
                p10.player.stop();
                p10.enemy.stop();
                JOptionPane.showMessageDialog(null, "Anda Kalah!!!");
                menuutama(gold);
            }
            p10.rpmlabel.setText(rpm + "");
        }
    }//GEN-LAST:event_formKeyReleased
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frame1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frame1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frame1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frame1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frame1().setVisible(true);
            }
        });
    }

    @Override
    public void run() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        while(true){
            try {
                //System.out.println(ctrthread);
                if(jalan==true){
                    jalan = false;
                }
                Thread.sleep(ctrthread);
                if(cekgerak==1){
                    //System.out.println("maju");
                    percepatan = (temp*pSpeed/50)*-1;
                    ctrthread+=percepatan;
                    rpm-=percepatan;
                    if(rpm>pSpeed)rpm=pSpeed;
                    if(ctrthread-pSpeed<=0){ctrthread = 500-pSpeed;}
                    temp++;
                    if(temp>10)temp = 10;
                }
                else if(cekgerak==0){
                    p10.rpmlabel.setText(rpm + "");
                    if(ctrthread < 500)p10.player.run();
                    //System.out.println("mundur");
                    percepatan = 15;
                    ctrthread+=percepatan;
                    rpm-=percepatan;
                    if(rpm<0)rpm=0;
                    if(ctrthread>=500){ctrthread = 500;}
                    temp--;
                    if(temp<1)temp = 1;
                }
                else if(cekgerak==2){
                    //System.out.println("mundur");
                    percepatan = 50;
                    ctrthread+=percepatan;
                    rpm-=percepatan;
                    if(rpm<0)rpm=0;
                    if(ctrthread>=500){ctrthread = 500;}
                    temp--;
                    if(temp<1)temp = 1;
                }
                else if(cekgerak==3){
                    //System.out.println("nitro");
                    percepatan = -100;
                    ctrthread+=percepatan;
                    rpm-=percepatan;
                    if(rpm>pSpeed)rpm=pSpeed;
                    if(ctrthread-pSpeed<=0){ctrthread = 500-pSpeed;}
                    temp+=4;
                    if(temp>10)temp = 10;
                }
            } catch (Exception e) {
            }
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
