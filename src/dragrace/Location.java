/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dragrace;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.io.Serializable;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author Albert
 */
public class Location extends Thread{
    int x,miles;
    ImageIcon[] gambar;
    JLabel lblUnit;
    
    public Location(int x){
        this.x = 0;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }
    
    /*public void Go(int speed){
        x = (int) (x + speed*0.1 + x*0.1);
    }*/    
}

class player extends Location implements Runnable{
    ImageIcon[] gambarx = new ImageIcon[3];
    animasi_player anim;
    public int miles;
    public player(int x,ImageIcon gambar,int miles) {
        
        super(x);
        this.miles = miles;
        ImageIcon raw = gambar;
        Image imgx = raw.getImage();
        Image newImgx = imgx.getScaledInstance(200, 100, Image.SCALE_SMOOTH);
        this.gambarx[0] = new ImageIcon(newImgx);
        
        raw = gambar;
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(200, 100, Image.SCALE_SMOOTH);
        this.gambarx[1] = new ImageIcon(newImgx);
        
        raw = gambar;
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(200, 100, Image.SCALE_SMOOTH);
        this.gambarx[2] = new ImageIcon(newImgx);
        
        this.lblUnit = new JLabel(this.gambarx[0]);
        this.lblUnit.setBounds(x, 130, 200, 100);
    }
    @Override
    public void run(){
        anim=new animasi_player(lblUnit, gambarx, miles);
        anim.start();
    }
}

class musuh extends Location implements Runnable{
    ImageIcon[] gambarx = new ImageIcon[3];
    animasi_musuh anim;
    int a = 500;
    int miles;
    public musuh(int x,ImageIcon gambar, int miles) {
        
        super(x);
        this.miles = miles;
        ImageIcon raw = gambar;
        Image imgx = raw.getImage();
        Image newImgx = imgx.getScaledInstance(200, 100, Image.SCALE_SMOOTH);
        this.gambarx[0] = new ImageIcon(newImgx);
        
        raw = gambar;
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(200, 100, Image.SCALE_SMOOTH);
        this.gambarx[1] = new ImageIcon(newImgx);
        
        raw = gambar;
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(200, 100, Image.SCALE_SMOOTH);
        this.gambarx[2] = new ImageIcon(newImgx);
        
        this.lblUnit = new JLabel(this.gambarx[0]);
        this.lblUnit.setBounds(x, 530, 200, 100);
    }
    @Override
    public void run() {
        while(true){
            //System.out.println("a = " + a);
            anim=new animasi_musuh(lblUnit, gambarx,miles);
            anim.start();

            try {
                Thread.sleep(a);//miliseconds
                a-=25;
                if(a<=300)a=300;
            } catch (InterruptedException ex) {

            }
            
        }
    }
}

class animasi_player extends java.lang.Thread implements Serializable{
    JLabel lblUnit;
    ImageIcon [] gambar = new ImageIcon[3];
    int ctrGerak,cmd,miles;

    public animasi_player(JLabel lblUnit, ImageIcon[] gambar,int miles) {
        this.lblUnit = lblUnit;
        this.ctrGerak = 0;
        this.gambar = gambar;
        this.miles = miles;
    }
    
    @Override
    public void run() {
        //super.run(); //To change body of generated methods, choose Tools | Templates.
        while(ctrGerak < 5){
            if(ctrGerak == 5){
                this.lblUnit.setIcon(gambar[1]);
            }
            else if(ctrGerak %2 == 0){
                this.lblUnit.setIcon(gambar[0]);
            }
            else if(ctrGerak %2 != 0){
                this.lblUnit.setIcon(gambar[2]);
            }
            ctrGerak++;
            setLokasi();
            try {
                Thread.sleep(100);//miliseconds
            } catch (InterruptedException ex) {
            }
        }
    }
    public void setLokasi(){
        lblUnit.setLocation(lblUnit.getX() + miles, lblUnit.getY());
    }
}

class animasi_musuh extends java.lang.Thread implements Serializable{
    JLabel lblUnit;
    ImageIcon [] gambar = new ImageIcon[3];
    int ctrGerak,cmd,miles;

    public animasi_musuh(JLabel lblUnit, ImageIcon[] gambar,int miles) {
        this.lblUnit = lblUnit;
        this.ctrGerak = 0;
        this.gambar = gambar;
        this.miles = miles;
    }
    
    @Override
    public void run() {
        //super.run(); //To change body of generated methods, choose Tools | Templates.
        while(ctrGerak < 5){
            if(ctrGerak == 5){
                this.lblUnit.setIcon(gambar[1]);
            }
            else if(ctrGerak %2 == 0){
                this.lblUnit.setIcon(gambar[0]);
            }
            else if(ctrGerak %2 != 0){
                this.lblUnit.setIcon(gambar[2]);
            }
            ctrGerak++;
            setLokasi();
            try {
                Thread.sleep(100);//miliseconds
            } catch (InterruptedException ex) {
            }
        }
    }
    public void setLokasi(){
        lblUnit.setLocation(lblUnit.getX() + miles, lblUnit.getY());
    }
}